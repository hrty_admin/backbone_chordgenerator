(function(window, $){
	"use strict";

	var CUSTOM_UTIL = {};
	CUSTOM_UTIL.isTouch = ('ontouchstart' in window);
	CUSTOM_UTIL.action = {
		start: CUSTOM_UTIL.isTouch ? 'touchstart' : 'mousedown',
		move: CUSTOM_UTIL.isTouch ? 'touchmove' : 'mousemove',
		end: CUSTOM_UTIL.isTouch ? 'touchend' : 'mouseup'
	};

	window.CUSTOM_UTIL = CUSTOM_UTIL;

	window.ClickClass = {};

	ClickClass = function(fn, cont) {
		var that = this,
			isTouch = false;
		var ACTION = CUSTOM_UTIL.action;

		var duration = CUSTOM_UTIL.isTouch ? 220 : 0;

		that.on(ACTION.start, function(e) {
			isTouch = true;
			that.addClass('tap');
		});
		that.on(ACTION.move, function(e) {
			if($(e.target).get(0) == that.get(0)) {
				e.preventDefault();
			}
			// 即キャンセル回避
			setTimeout(function() {
				isTouch = false;
				that.removeClass('tap');
			}, duration);
		});
		that.on(ACTION.end, function(e) {
			if(isTouch && $(e.target).get(0) == that.get(0)) {
				e.preventDefault();
				fn.apply(cont);
				that.removeClass('tap');
			}
		});
	};

	// androidはheaderを固定しない
	// TODO: 応急処置なので要対応
	window.addEventListener('DOMContentLoaded', function() {
		if(navigator.userAgent.indexOf('Android') > 0){
		    var css = document.createElement('style'),
		    	rule = document.createTextNode('#slidemenu-header-container{position:static;-webkit-box-shadow:none;display:block;height:53px;}#slidemenu-header-container.show{-webkit-transform: none;position:relative;}#slidemenu-main{padding-top:0;}.slidemenu{padding-top:53px;}');
		    if (css.styleSheet) {
		        css.styleSheet.cssText = rule.nodeValue;
		    } else {
		        css.appendChild(rule);
		    };
		    document.getElementsByTagName('head')[0].appendChild(css);
	    }
	}, false);

})(this, jQuery);





/* ********************
// chord generator localstorage
// ****************** */
(function(window, document, $){
	"use strict";

	window.CHORD_LOCALSTORAGE_PREFIX = "chordStorage-";

	var	CHORD_GENERATOR_URL = "/chord/chord-generator.html",
		DATA_CHORD_LIST = "[data-chord-list]",
		DATA_SAVED_CHORDS = "[data-saved-chords]",
		CHORD_LIST_CLASS = "list-main",
		DELETE_CHORD_LIST_CLASS = "list-status delete-cord-list",
		CHORD_EMPTY_MSG = "<div class='post-area' style='margin-top: 18px;'><p>まだ保存されたコードはありません。<br>コード表ジェネレーターは<a href='/chord/chord-generator.html' class='color hover'>こちら</a></p><div class='sub-wrap-box type-radius dotted'><span class='summary-chord-generator'>コード表ジェネレーターとは</span><p>コード一覧をすぐに確かめられるだけでなく、自分だけのコード進行を作成し、簡単に保存しておけるツールです。</p></div></div>";

	// chord-generator-app用
	window.ShowChordStorage = {};

	var ChordStorage = function(elm) {
		this.elm = elm;
		this.chordsNum;
		this.init();
	};

	ChordStorage.prototype = {
		init: function() {
			var _localStorage = localStorage,
				isChordStorage = false,
				i = 0;

			var ul = document.createElement('ul'),
				li = document.createElement('li'),
				a = document.createElement('a'),
				span = document.createElement('span'),
				liFrag = document.createDocumentFragment();

			this.elm.innerHTML = "";
			this.chordsNum = 0;

			for(i; i < _localStorage.length; i++) {
				var that = this,
					chordName = _localStorage.key(i),
					chordNum = _localStorage[chordName];

				if(chordName.indexOf(CHORD_LOCALSTORAGE_PREFIX) !== -1) {
					var _li = li.cloneNode(true),
						_a = a.cloneNode(true),
						_span = span.cloneNode(true);

					var _chordName = chordName.replace(CHORD_LOCALSTORAGE_PREFIX, "")

					_a.href = CHORD_GENERATOR_URL + "#" + chordNum;
					_a.textContent = _chordName;

					_span.className = DELETE_CHORD_LIST_CLASS;
					_span.addEventListener('click', (function(chordName) {
						return function() {
							that.deleteListChord(chordName);
						}
					})(chordName), false);

					_li.appendChild(_a);
					_li.appendChild(_span);

					liFrag.insertBefore(_li, liFrag.firstChild);

					isChordStorage = true;

					this.chordsNum++;
				}
			}

			if(isChordStorage) {
				ul.className = CHORD_LIST_CLASS;
				ul.appendChild(liFrag);
				this.showListChord(ul);
			} else {
				this.showEmptyMsg();
			}

			this.setSavedChords();
		},
		showListChord: function(ul) {
			this.elm.appendChild(ul);
		},
		showEmptyMsg: function() {
			this.elm.innerHTML = CHORD_EMPTY_MSG;
		},
		deleteListChord: function(n) {
			localStorage.removeItem(n);
			this.init();
			return false;
		},
		setSavedChords: function() {
			var savedChordsTarget = document.querySelectorAll(DATA_SAVED_CHORDS),
				i = 0;

			if(this.chordsNum > 0) {
				$(savedChordsTarget).removeClass('disnone');

				for(i; i < savedChordsTarget.length; i++) {
					savedChordsTarget[i].innerHTML = this.chordsNum;
				}
			} else {
				$(savedChordsTarget).addClass('disnone');
			}

		}
	};

	ShowChordStorage = function() {
		var chordListTarget = document.querySelectorAll(DATA_CHORD_LIST),
			i = 0;

		for(i; i < chordListTarget.length; i++) {
			new ChordStorage(chordListTarget[i]);
		}
	};

	document.addEventListener('DOMContentLoaded', function() {
		ShowChordStorage();
	}, false);

})(this, this.document, jQuery);




/* ********************
// modal
// ****************** */
(function(window, document, $){
	"use strict";

	var MODAL_SHIELD = $('#jmodal'),
		MODAL_CONTENT = $('#jmodal-content'),
		MODAL_CLOSE_BTN = $('#jmodal-close'),
		MODAL_BTN = "data-modal",
		DISPLAY_NONE_CLASS = "disnone",
		ANIMATION_CLASS = "show-anim";

	var ACTION = CUSTOM_UTIL.action;

	var Modal = function(elm) {
		var _targetId = elm.getAttribute(MODAL_BTN);

		this.$elm = $(elm);
		this.$target = $(_targetId);
		this.$shield = MODAL_SHIELD;
		this.$content = MODAL_CONTENT;
		this.$closeBtn = MODAL_CLOSE_BTN;
		this.isShow = false;

		this.init();
	};

	Modal.prototype = {
		init: function() {
			var that = this,
				isTouch = false,
				$_elm = this.$elm,
				$_shield = this.$shield,
				$_closeBtn = this.$closeBtn;

			// show
			ClickClass.apply($_elm, [that.show, that]);

			// hide
			ClickClass.apply($_shield, [that.hide, that]);

			$_closeBtn.on(ACTION.end, function(e) {
				e.preventDefault();
				that.hide();
			});
		},
		show: function() {
			var that = this;
			if(!this.isShow) {
				this.$content.css("top", $(window).scrollTop() + 50 + "px");

				this.$shield.removeClass(DISPLAY_NONE_CLASS);
				this.$target.removeClass(DISPLAY_NONE_CLASS);
				that.isShow = true;
				setTimeout(function() {
					that.$content.addClass(ANIMATION_CLASS);
				}, 10);

				// info notice 既読
				if (this.$elm.get(0).getAttribute(MODAL_BTN) == "#modal-info") {
					window.ShowInfoNotice(true);
				}
			}
		},
		hide: function() {
			if(this.isShow) {
				this.$shield.addClass(DISPLAY_NONE_CLASS);
				this.$target.addClass(DISPLAY_NONE_CLASS);
				this.$content.removeClass(ANIMATION_CLASS);
				this.isShow = false;
			}
		}
	};

	document.addEventListener('DOMContentLoaded', function() {
		var modalBtn = document.querySelectorAll("[" + MODAL_BTN + "]"),
			i = 0;

		for(i; i < modalBtn.length; i++) {
			new Modal(modalBtn[i]);
		}
	}, false);

})(this, this.document, jQuery);




/* ********************
// save msg
// ****************** */

(function(window, document, $){
	"use strict";

	window.showMsg = {};

	var isShow = false,
		showTime = 3000,
		openMsgClass = "msg-show",
		displayNoneClass = "disnone";

	showMsg = function(t) {
		var $target = $(t);

		if(!isShow) {
			$target.removeClass(displayNoneClass);
			$target.addClass(openMsgClass);
			isShow = true;
			setTimeout(function() {
				$target.removeClass(openMsgClass);
				isShow =false;
				$target.on('webkitTransitionEnd oTransitionEnd otransitionend transitionend', function() {
					$target.addClass(displayNoneClass);
				});
			}, showTime);
		}
	};

})(this, this.document, jQuery);






/* ********************
// chord url
// ****************** */

(function(window, document, $) {
	'use strict';

	var DIATONIC = {
		major: {
			key: [0,2,4,5,7,9,11],
			type: [1,7,7,1,2,7,9]
		},
		minor: {
			key: [0,2,3,5,7,8,10],
			type: [7,9,1,7,7,1,2]
		},
		melodic: {
			key: [0,2,4,5,7,9,11],
			type: [1,7,7,1,2,7,9]
		},
		harmonic: {
			key: [0,2,4,5,7,9,11],
			type: [1,7,7,1,2,7,9]
		}
	}

	var ChordUrl = function(tgt) {
		this.tgt = tgt;
		this.obj = JSON.parse(this.tgt.getAttribute(ChordUrl.TARGET_ATTRIBUTE));
		this.url = "";

		this.createUrl();
	};

	ChordUrl.TARGET_ATTRIBUTE = "data-chord-url";

	ChordUrl.prototype = {
		createUrl: function() {
			var i = 0,
				o = this.obj,
				u = this.url,
				d;

			if(o.type == "major") {
				d = DIATONIC.major;
			} else if(o.type == "minor") {
				d = DIATONIC.minor;
			} else if(o.type == "melodic") {
				d = DIATONIC.melodic;
			} else if(o.type == "harmonic") {
				d = DIATONIC.harmonic;
			}

			for(i; i < d.key.length; i++) {
				var _key = d.key[i] + o.key;
				if(_key >= 12) {_key -= 12};
				u += "-" + _key + "_" + d.type[i];
			}

			this.url = u.substr(1);
		},
		setUrl: function() {
			var _href = this.tgt.href;
			this.tgt.href = _href + '#' + this.url;
		}
	}

	window.ChordUrl = ChordUrl;

})(this, this.document, jQuery);




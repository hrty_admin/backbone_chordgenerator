/* ********************
// IE8以下非対象
// ****************** */

if(typeof window.addEventListener == "undefined" && typeof document.getElementsByClassName == "undefined"){

	var newElem = document.createElement("div");
	newElem.innerHTML = '<div style="padding: 35px 0;background: #fff;text-align: center;"><div class="post-area" style="margin-bottom: 0 !important;"><p>あなたは古いInternet Explorerをご利用中であり、大変危険な脆弱性が確認されています。<br />セキュリティを向上させるため、またウェブサイトを快適に閲覧するために、<br />ブラウザの無料バージョンアップを強くお勧めします。</p><p><a href="http://www.google.com/chrome/intl/ja/landing.html" target="_blank" class="blank">最も人気の高いブラウザ、Google Chromeのダウンロードはこちら</a><br /><a href="http://www.microsoft.com/ja-jp/windows/products/winfamily/ie/function/default.aspx" target="_blank" class="blank">最新版のInternet Explorerのダウンロードはこちら</a></p></div></div>';
	document.body.insertBefore(newElem, document.body.firstChild);

}
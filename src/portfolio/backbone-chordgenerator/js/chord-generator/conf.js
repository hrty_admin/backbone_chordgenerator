
;(function(){

	"use strict";

// ********************
// global登録
// ********************

	window.FINGER_BORD = {}; // fingerbord情報
	window.CHORD_CONF = {}; // コード情報
	window.CHORD_CONFIG_DATA = {}; // コードパターン（JSON）

// ********************
// fingerbord情報
// ********************

	FINGER_BORD =  {
		FLET_X: 22, // フレットの描画基点
		FLET_Y: 10,
		CANVAS_WIDTH: 320, // canvasサイズ
		CANVAS_HEIGHT: 220,
		FLET_INTERVAL: 30, // フレット間の距離
		STRING_INTERVAL: 15, // 弦間の距離
		FLET_NUMBER: 4, // 4フレット分を表示
		FLET_LINEWIDTH: 2, // フレットの太さ
		ARC_RADIUS: 6, // 押弦の半径
		FLET_COLOR: "#666", // フレットの色
		STRING_COLOR: "#dadada", // 弦の色
		ARC_COLOR: "#E3587A", // 押弦の色
		FLET_NUMBER_COLOR: "#666" // フレットナンバーの色
	}

// ********************
// コード情報
// ********************

	CHORD_CONF = {
		root: [
			{
				key: 0,
				name: "C",
				pronunciation: "シー"
			},
			{
				key: 1,
				name: "C#",
				pronunciation: "シーシャープ"
			},
			{
				key: 2,
				name: "D",
				pronunciation: "ディー"
			},
			{
				key: 3,
				name: "E♭",
				pronunciation: "イーフラット"
			},
			{
				key: 4,
				name: "E",
				pronunciation: "イー"
			},
			{
				key: 5,
				name: "F",
				pronunciation: "エフ"
			},
			{
				key: 6,
				name: "F#",
				pronunciation: "エフシャープ"
			},
			{
				key: 7,
				name: "G",
				pronunciation: "ジー"
			},
			{
				key: 8,
				name: "G#",
				pronunciation: "ジーシャープ"
			},
			{
				key: 9,
				name: "A",
				pronunciation: "エー"
			},
			{
				key: 10,
				name: "B♭",
				pronunciation: "ビーフラット"
			},
			{
				key: 11,
				name: "B",
				pronunciation: "ビー"
			}
		],
		type: [
			{
				key: 0,
				name: "M",
				pronunciation: "メジャー"
			},
			{
				key: 1,
				name: "M7",
				pronunciation: "メジャーセブン"
			},
			{
				key: 2,
				name: "7",
				pronunciation: "セブンス"
			},
			{
				key: 3,
				name: "6",
				pronunciation: "シックス"
			},
			{
				key: 4,
				name: "aug",
				pronunciation: "オーギュメント"
			},
			{
				key: 5,
				name: "m",
				pronunciation: "マイナー"
			},
			{
				key: 6,
				name: "mM7",
				pronunciation: "マイナーメジャーセブン"
			},
			{
				key: 7,
				name: "m7",
				pronunciation: "マイナーセブン"
			},
			{
				key: 8,
				name: "m6",
				pronunciation: "マイナーシックス"
			},
			{
				key: 9,
				name: "m7(♭5)",
				pronunciation: "マイナーセブンフラットファイブ"
			},
			{
				key: 10,
				name: "add9",
				pronunciation: "アドナイン"
			},
			{
				key: 11,
				name: "sus4",
				pronunciation: "サスフォー"
			},
			{
				key: 12,
				name: "7sus4",
				pronunciation: "セブンスサスフォー"
			},
			{
				key: 13,
				name: "7(♭9)",
				pronunciation: "セブンスフラットナイン"
			},
			{
				key: 14,
				name: "7(9)",
				pronunciation: "セブンスナイン"
			},
			{
				key: 15,
				name: "7(#9)",
				pronunciation: "セブンスシャープナイン"
			},
			{
				key: 16,
				name: "dim7",
				pronunciation: "ディミニッシュ"
			}
		]
	};

// ********************
// コードパターン
// [1st,2nd,3rd,4th,5th,6th,bar_pos,bar_from,bar_to]
// ********************

	// 5弦ルート定型
	var	_R5_M = [1,3,3,3,1,-1,1,1,5,'root5'];
	var	_R5_M7 = [1,3,2,3,1,-1,1,1,5,'root5'];
	var _R5_7 = [1,3,1,3,1,-1,1,1,5,'root5'];
	var _R5_6 = [-1,1,2,2,3,-1,,,,'root5'];
	var _R5_aug = [-1,1,1,2,3,-1,1,2,3,'root5'];
	var	_R5_m = [1,2,3,3,1,-1,1,1,5,'root5'];
	var	_R5_mM7 = [1,2,2,3,1,-1,1,1,5,'root5'];
	var	_R5_m7 = [1,2,1,3,1,-1,1,1,5,'root5'];
	var	_R5_m6 = [3,1,2,1,3,-1,1,2,4,'root5'];
	var	_R5_m7_5 = [-1,2,1,2,1,-1,,,,'root5'];
	var	_R5_add9 = [1,1,3,3,1,-1,1,1,5,'root5'];
	var	_R5_sus4 = [1,4,3,3,1,-1,1,1,5,'root5'];
	var	_R5_7sus4 = [1,4,1,3,1,-1,1,1,5,'root5'];
	var	_R5_7_9 = [-1,1,2,1,2,-1,1,2,4,'root5'];
	var	_R5_79 = [-1,2,2,1,2,-1,,,,'root5'];
	var	_R5_7__9 = [-1,3,2,1,2,-1,,,,'root5'];
	var	_R5_dim7 = [-1,3,1,3,2,-1,,,,'root5'];

	// 6弦ルート定型
	var _R6_M = [1,1,2,3,3,1,1,1,6,'root6'];
	var _R6_M7 = [1,1,2,2,3,1,1,1,6,'root6'];
	var _R6_7 = [1,1,2,1,3,1,1,1,6,'root6'];
	var _R6_6 = [-1,2,3,1,-1,2,,,,'root6'];
	var _R6_aug = [1,2,2,3,-1,-1,,,,'root6'];
	var	_R6_m = [1,1,1,3,3,1,1,1,6,'root6'];
	var	_R6_mM7 = [1,1,1,2,3,1,1,1,6,'root6'];
	var	_R6_m7 = [1,1,1,1,3,1,1,1,6,'root6'];
	var	_R6_m6 = [-1,2,2,1,-1,2,,,,'root6'];
	var	_R6_m7_5 = [-1,1,2,2,-1,2,,,,'root6'];
	var	_R6_sus4 = [1,1,3,3,3,1,1,1,6,'root6'];
	var	_R6_7sus4 = [1,1,3,1,3,1,1,1,6,'root6'];
	var _R6_7_9 = [2,1,2,1,3,1,1,2,6,'root6'];
	var _R6_79 = [3,1,2,1,3,1,1,2,6,'root6'];
	var _R6_7__9 = [4,1,2,1,3,1,1,2,6,'root6'];
	var	_R6_dim7 = [-1,1,2,1,-1,2,1,2,4,'root6'];


	// JSONデータ
	CHORD_CONFIG_DATA = {
		"C": {
			"M": [
				{
					"pos": 3,
					"chord_number": _R5_M
				},
				{
					"pos": 8,
					"chord_number": _R6_M
				},
				{
					"pos": 1,
					"chord_number": [0,1,0,2,3,-1,,,,'row']
				}
			],
			"M7": [
				{
					"pos": 3,
					"chord_number": _R5_M7
				},
				{
					"pos": 8,
					"chord_number": _R6_M7
				},
				{
					"pos": 1,
					"chord_number": [0,0,0,2,3,-1,,,,'row']
				}
			],
			"7": [
				{
					"pos": 3,
					"chord_number": _R5_7
				},
				{
					"pos": 8,
					"chord_number": _R6_7
				},
				{
					"pos": 1,
					"chord_number": [0,1,3,2,3,-1,,,,'row']
				}
			],
			"6": [
				{
					"pos": 1,
					"chord_number": _R5_6
				},
				{
					"pos": 7,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 1,
					"chord_number": _R5_aug
				},
				{
					"pos": 8,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 3,
					"chord_number": _R5_m
				},
				{
					"pos": 8,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 3,
					"chord_number": _R5_mM7
				},
				{
					"pos": 8,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 3,
					"chord_number": _R5_m7
				},
				{
					"pos": 8,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 1,
					"chord_number": _R5_m6
				},
				{
					"pos": 7,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 3,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 7,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 3,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 3,
					"chord_number": _R5_sus4
				},
				{
					"pos": 8,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 3,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 8,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 2,
					"chord_number": _R5_7_9
				},
				{
					"pos": 8,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 2,
					"chord_number": _R5_79
				},
				{
					"pos": 8,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 2,
					"chord_number": _R5_7__9
				},
				{
					"pos": 8,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 2,
					"chord_number": _R5_dim7
				},
				{
					"pos": 8,
					"chord_number": _R6_dim7
				}
			]
		},
		"C#": {
			"M": [
				{
					"pos": 4,
					"chord_number": _R5_M
				},
				{
					"pos": 9,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 4,
					"chord_number": _R5_M7
				},
				{
					"pos": 9,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 4,
					"chord_number": _R5_7
				},
				{
					"pos": 9,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 2,
					"chord_number": _R5_6
				},
				{
					"pos": 8,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 2,
					"chord_number": _R5_aug
				},
				{
					"pos": 9,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 4,
					"chord_number": _R5_m
				},
				{
					"pos": 9,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 4,
					"chord_number": _R5_mM7
				},
				{
					"pos": 9,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 4,
					"chord_number": _R5_m7
				},
				{
					"pos": 9,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 2,
					"chord_number": _R5_m6
				},
				{
					"pos": 8,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 4,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 8,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 4,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 4,
					"chord_number": _R5_sus4
				},
				{
					"pos": 9,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 4,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 9,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 3,
					"chord_number": _R5_7_9
				},
				{
					"pos": 9,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 3,
					"chord_number": _R5_79
				},
				{
					"pos": 9,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 3,
					"chord_number": _R5_7__9
				},
				{
					"pos": 9,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 3,
					"chord_number": _R5_dim7
				},
				{
					"pos": 9,
					"chord_number": _R6_dim7
				}
			]
		},
		"D": {
			"M": [
				{
					"pos": 5,
					"chord_number": _R5_M
				},
				{
					"pos": 10,
					"chord_number": _R6_M
				},
				{
					"pos": 1,
					"chord_number": [2,3,2,0,-1,-1,,,,'row']
				}
			],
			"M7": [
				{
					"pos": 5,
					"chord_number": _R5_M7
				},
				{
					"pos": 10,
					"chord_number": _R6_M7
				},
				{
					"pos": 1,
					"chord_number": [2,2,2,0,-1,-1,,,,'row']
				}
			],
			"7": [
				{
					"pos": 5,
					"chord_number": _R5_7
				},
				{
					"pos": 10,
					"chord_number": _R6_7
				},
				{
					"pos": 1,
					"chord_number": [2,1,2,0,-1,-1,,,,'row']
				}
			],
			"6": [
				{
					"pos": 3,
					"chord_number": _R5_6
				},
				{
					"pos": 9,
					"chord_number": _R6_6
				},
				{
					"pos": 1,
					"chord_number": [2,0,2,0,-1,-1,,,,'row']
				}
			],
			"aug": [
				{
					"pos": 3,
					"chord_number": _R5_aug
				},
				{
					"pos": 10,
					"chord_number": _R6_aug
				},
				{
					"pos": 1,
					"chord_number": [2,3,3,0,-1,-1,,,,'row']
				}
			],
			"m": [
				{
					"pos": 5,
					"chord_number": _R5_m
				},
				{
					"pos": 10,
					"chord_number": _R6_m
				},
				{
					"pos": 1,
					"chord_number": [1,3,2,0,-1,-1,,,,'row']
				}
			],
			"mM7": [
				{
					"pos": 5,
					"chord_number": _R5_mM7
				},
				{
					"pos": 10,
					"chord_number": _R6_mM7
				},
				{
					"pos": 1,
					"chord_number": [1,2,2,0,-1,-1,,,,'row']
				}
			],
			"m7": [
				{
					"pos": 5,
					"chord_number": _R5_m7
				},
				{
					"pos": 10,
					"chord_number": _R6_m7
				},
				{
					"pos": 1,
					"chord_number": [1,1,2,0,-1,-1,,,,'row']
				}
			],
			"m6": [
				{
					"pos": 3,
					"chord_number": _R5_m6
				},
				{
					"pos": 9,
					"chord_number": _R6_m6
				},
				{
					"pos": 1,
					"chord_number": [1,0,2,0,-1,-1,,,,'row']
				}
			],
			"m7(♭5)": [
				{
					"pos": 5,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 9,
					"chord_number": _R6_m7_5
				},
				{
					"pos": 1,
					"chord_number": [1,1,1,0,-1,-1,,,,'row']
				}
			],
			"add9": [
				{
					"pos": 5,
					"chord_number": _R5_add9
				},
				{
					"pos": 1,
					"chord_number": [0,3,2,0,-1,-1,,,,'row']
				}
			],
			"sus4": [
				{
					"pos": 5,
					"chord_number": _R5_sus4
				},
				{
					"pos": 10,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 5,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 10,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 4,
					"chord_number": _R5_7_9
				},
				{
					"pos": 10,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 4,
					"chord_number": _R5_79
				},
				{
					"pos": 10,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 4,
					"chord_number": _R5_7__9
				},
				{
					"pos": 10,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 4,
					"chord_number": _R5_dim7
				},
				{
					"pos": 9,
					"chord_number": _R6_dim7
				},
				{
					"pos": 1,
					"chord_number": [1,0,1,0,-1,-1,,,,'row']
				}
			]
		},
		"E♭": {
			"M": [
				{
					"pos": 6,
					"chord_number": _R5_M
				},
				{
					"pos": 11,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 6,
					"chord_number": _R5_M7
				},
				{
					"pos": 11,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 6,
					"chord_number": _R5_7
				},
				{
					"pos": 11,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 4,
					"chord_number": _R5_6
				},
				{
					"pos": 10,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 4,
					"chord_number": _R5_aug
				},
				{
					"pos": 11,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 6,
					"chord_number": _R5_m
				},
				{
					"pos": 11,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 6,
					"chord_number": _R5_mM7
				},
				{
					"pos": 11,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 6,
					"chord_number": _R5_m7
				},
				{
					"pos": 11,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 4,
					"chord_number": _R5_m6
				},
				{
					"pos": 10,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 6,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 10,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 6,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 6,
					"chord_number": _R5_sus4
				},
				{
					"pos": 11,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 6,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 11,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 5,
					"chord_number": _R5_7_9
				},
				{
					"pos": 11,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 5,
					"chord_number": _R5_79
				},
				{
					"pos": 11,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 5,
					"chord_number": _R5_7__9
				},
				{
					"pos": 11,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 5,
					"chord_number": _R5_dim7
				},
				{
					"pos": 10,
					"chord_number": _R6_dim7
				}
			]
		},
		"E": {
			"M": [
				{
					"pos": 7,
					"chord_number": _R5_M
				},
				{
					"pos": 12,
					"chord_number": _R6_M
				},
				{
					"pos": 1,
					"chord_number": [0,0,1,2,2,0,,,,'row']
				}
			],
			"M7": [
				{
					"pos": 7,
					"chord_number": _R5_M7
				},
				{
					"pos": 12,
					"chord_number": _R6_M7
				},
				{
					"pos": 1,
					"chord_number": [0,0,1,1,2,0,,,,'row']
				}
			],
			"7": [
				{
					"pos": 7,
					"chord_number": _R5_7
				},
				{
					"pos": 12,
					"chord_number": _R6_7
				},
				{
					"pos": 1,
					"chord_number": [0,0,1,0,2,0,,,,'row']
				}
			],
			"6": [
				{
					"pos": 5,
					"chord_number": _R5_6
				},
				{
					"pos": 11,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 5,
					"chord_number": _R5_aug
				},
				{
					"pos": 12,
					"chord_number": _R6_aug
				},
				{
					"pos": 1,
					"chord_number": [0,1,1,2,-1,-1,,,,'row']
				}
			],
			"m": [
				{
					"pos": 7,
					"chord_number": _R5_m
				},
				{
					"pos": 12,
					"chord_number": _R6_m
				},
				{
					"pos": 1,
					"chord_number": [0,0,0,2,2,0,,,,'row']
				}
			],
			"mM7": [
				{
					"pos": 7,
					"chord_number": _R5_mM7
				},
				{
					"pos": 12,
					"chord_number": _R6_mM7
				},
				{
					"pos": 1,
					"chord_number": [0,0,0,1,2,0,,,,'row']
				}
			],
			"m7": [
				{
					"pos": 7,
					"chord_number": _R5_m7
				},
				{
					"pos": 12,
					"chord_number": _R6_m7
				},
				{
					"pos": 1,
					"chord_number": [0,0,0,0,2,0,,,,'row']
				}
			],
			"m6": [
				{
					"pos": 5,
					"chord_number": _R5_m6
				},
				{
					"pos": 11,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 7,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 11,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 7,
					"chord_number": _R5_add9
				},
				{
					"pos": 1,
					"chord_number": [2,0,1,2,-1,-1,,,,'row']
				}
			],
			"sus4": [
				{
					"pos": 7,
					"chord_number": _R5_sus4
				},
				{
					"pos": 12,
					"chord_number": _R6_sus4
				},
				{
					"pos": 1,
					"chord_number": [0,0,2,2,2,0,,,,'row']
				}
			],
			"7sus4": [
				{
					"pos": 7,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 12,
					"chord_number": _R6_7sus4
				},
				{
					"pos": 1,
					"chord_number": [0,0,2,0,2,0,,,,'row']
				}
			],
			"7(♭9)": [
				{
					"pos": 6,
					"chord_number": _R5_7_9
				},
				{
					"pos": 12,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 6,
					"chord_number": _R5_79
				},
				{
					"pos": 12,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 6,
					"chord_number": _R5_7__9
				},
				{
					"pos": 12,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 6,
					"chord_number": _R5_dim7
				},
				{
					"pos": 11,
					"chord_number": _R6_dim7
				}
			]
		},
		"F": {
			"M": [
				{
					"pos": 8,
					"chord_number": _R5_M
				},
				{
					"pos": 1,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 8,
					"chord_number": _R5_M7
				},
				{
					"pos": 1,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 8,
					"chord_number": _R5_7
				},
				{
					"pos": 1,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 6,
					"chord_number": _R5_6
				},
				{
					"pos": 12,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 6,
					"chord_number": _R5_aug
				},
				{
					"pos": 1,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 8,
					"chord_number": _R5_m
				},
				{
					"pos": 1,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 8,
					"chord_number": _R5_mM7
				},
				{
					"pos": 1,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 8,
					"chord_number": _R5_m7
				},
				{
					"pos": 1,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 6,
					"chord_number": _R5_m6
				},
				{
					"pos": 12,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 8,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 12,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 8,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 8,
					"chord_number": _R5_sus4
				},
				{
					"pos": 2,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 8,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 1,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 7,
					"chord_number": _R5_7_9
				},
				{
					"pos": 1,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 7,
					"chord_number": _R5_79
				},
				{
					"pos": 1,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 7,
					"chord_number": _R5_7__9
				},
				{
					"pos": 1,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 7,
					"chord_number": _R5_dim7
				},
				{
					"pos": 12,
					"chord_number": _R6_dim7
				}
			]
		},
		"F#": {
			"M": [
				{
					"pos": 9,
					"chord_number": _R5_M
				},
				{
					"pos": 2,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 9,
					"chord_number": _R5_M7
				},
				{
					"pos": 2,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 9,
					"chord_number": _R5_7
				},
				{
					"pos": 2,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 7,
					"chord_number": _R5_6
				},
				{
					"pos": 1,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 7,
					"chord_number": _R5_aug
				},
				{
					"pos": 2,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 9,
					"chord_number": _R5_m
				},
				{
					"pos": 2,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 9,
					"chord_number": _R5_mM7
				},
				{
					"pos": 2,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 9,
					"chord_number": _R5_m7
				},
				{
					"pos": 2,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 7,
					"chord_number": _R5_m6
				},
				{
					"pos": 1,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 9,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 1,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 9,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 9,
					"chord_number": _R5_sus4
				},
				{
					"pos": 3,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 9,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 2,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 8,
					"chord_number": _R5_7_9
				},
				{
					"pos": 2,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 8,
					"chord_number": _R5_79
				},
				{
					"pos": 2,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 8,
					"chord_number": _R5_7__9
				},
				{
					"pos": 2,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 8,
					"chord_number": _R5_dim7
				},
				{
					"pos": 1,
					"chord_number": _R6_dim7
				}
			]
		},
		"G": {
			"M": [
				{
					"pos": 10,
					"chord_number": _R5_M
				},
				{
					"pos": 3,
					"chord_number": _R6_M
				},
				{
					"pos": 1,
					"chord_number": [3,0,0,0,2,3,,,,'row']
				}
			],
			"M7": [
				{
					"pos": 10,
					"chord_number": _R5_M7
				},
				{
					"pos": 3,
					"chord_number": _R6_M7
				},
				{
					"pos": 1,
					"chord_number": [2,0,0,0,2,3,,,,'row']
				}
			],
			"7": [
				{
					"pos": 10,
					"chord_number": _R5_7
				},
				{
					"pos": 3,
					"chord_number": _R6_7
				},
				{
					"pos": 1,
					"chord_number": [1,0,0,0,2,3,,,,'row']
				}
			],
			"6": [
				{
					"pos": 8,
					"chord_number": _R5_6
				},
				{
					"pos": 2,
					"chord_number": _R6_6
				},
				{
					"pos": 1,
					"chord_number": [0,0,0,0,2,3,,,,'row']
				}
			],
			"aug": [
				{
					"pos": 8,
					"chord_number": _R5_aug
				},
				{
					"pos": 3,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 10,
					"chord_number": _R5_m
				},
				{
					"pos": 3,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 10,
					"chord_number": _R5_mM7
				},
				{
					"pos": 3,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 10,
					"chord_number": _R5_m7
				},
				{
					"pos": 3,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 8,
					"chord_number": _R5_m6
				},
				{
					"pos": 3,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 10,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 2,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 10,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 10,
					"chord_number": _R5_sus4
				},
				{
					"pos": 4,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 10,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 3,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 9,
					"chord_number": _R5_7_9
				},
				{
					"pos": 3,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 9,
					"chord_number": _R5_79
				},
				{
					"pos": 3,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 9,
					"chord_number": _R5_7__9
				},
				{
					"pos": 3,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 9,
					"chord_number": _R5_dim7
				},
				{
					"pos": 2,
					"chord_number": _R6_dim7
				}
			]
		},
		"G#": {
			"M": [
				{
					"pos": 11,
					"chord_number": _R5_M
				},
				{
					"pos": 4,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 11,
					"chord_number": _R5_M7
				},
				{
					"pos": 4,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 11,
					"chord_number": _R5_7
				},
				{
					"pos": 4,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 9,
					"chord_number": _R5_6
				},
				{
					"pos": 3,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 9,
					"chord_number": _R5_aug
				},
				{
					"pos": 4,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 11,
					"chord_number": _R5_m
				},
				{
					"pos": 4,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 11,
					"chord_number": _R5_mM7
				},
				{
					"pos": 4,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 11,
					"chord_number": _R5_m7
				},
				{
					"pos": 4,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 9,
					"chord_number": _R5_m6
				},
				{
					"pos": 4,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 11,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 3,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 11,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 11,
					"chord_number": _R5_sus4
				},
				{
					"pos": 5,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 11,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 4,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 10,
					"chord_number": _R5_7_9
				},
				{
					"pos": 4,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 10,
					"chord_number": _R5_79
				},
				{
					"pos": 4,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 10,
					"chord_number": _R5_7__9
				},
				{
					"pos": 4,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 10,
					"chord_number": _R5_dim7
				},
				{
					"pos": 3,
					"chord_number": _R6_dim7
				}
			]
		},
		"A": {
			"M": [
				{
					"pos": 12,
					"chord_number": _R5_M
				},
				{
					"pos": 5,
					"chord_number": _R6_M
				},
				{
					"pos": 1,
					"chord_number": [0,2,2,2,0,-1,,,,'row']
				}
			],
			"M7": [
				{
					"pos": 12,
					"chord_number": _R5_M7
				},
				{
					"pos": 5,
					"chord_number": _R6_M7
				},
				{
					"pos": 1,
					"chord_number": [0,2,1,2,0,-1,,,,'row']
				}
			],
			"7": [
				{
					"pos": 12,
					"chord_number": _R5_7
				},
				{
					"pos": 5,
					"chord_number": _R6_7
				},
				{
					"pos": 1,
					"chord_number": [0,2,0,2,0,-1,,,,'row']
				}
			],
			"6": [
				{
					"pos": 10,
					"chord_number": _R5_6
				},
				{
					"pos": 4,
					"chord_number": _R6_6
				},
				{
					"pos": 1,
					"chord_number": [2,2,2,2,0,-1,,,,'row']
				}
			],
			"aug": [
				{
					"pos": 10,
					"chord_number": _R5_aug
				},
				{
					"pos": 5,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 12,
					"chord_number": _R5_m
				},
				{
					"pos": 5,
					"chord_number": _R6_m
				},
				{
					"pos": 1,
					"chord_number": [0,1,2,2,0,-1,,,,'row']
				}
			],
			"mM7": [
				{
					"pos": 12,
					"chord_number": _R5_mM7
				},
				{
					"pos": 5,
					"chord_number": _R6_mM7
				},
				{
					"pos": 1,
					"chord_number": [0,1,1,2,0,-1,,,,'row']
				}
			],
			"m7": [
				{
					"pos": 12,
					"chord_number": _R5_m7
				},
				{
					"pos": 5,
					"chord_number": _R6_m7
				},
				{
					"pos": 1,
					"chord_number": [0,1,0,2,0,-1,,,,'row']
				}
			],
			"m6": [
				{
					"pos": 10,
					"chord_number": _R5_m6
				},
				{
					"pos": 5,
					"chord_number": _R6_m6
				},
				{
					"pos": 1,
					"chord_number": [2,1,2,2,0,-1,,,,'row']
				}
			],
			"m7(♭5)": [
				{
					"pos": 12,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 4,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 12,
					"chord_number": _R5_add9
				},
				{
					"pos": 1,
					"chord_number": [0,0,2,2,0,-1,,,,'row']
				}
			],
			"sus4": [
				{
					"pos": 12,
					"chord_number": _R5_sus4
				},
				{
					"pos": 6,
					"chord_number": _R6_sus4
				},
				{
					"pos": 1,
					"chord_number": [0,3,2,2,0,-1,,,,'row']
				}
			],
			"7sus4": [
				{
					"pos": 12,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 4,
					"chord_number": _R6_7sus4
				},
				{
					"pos": 1,
					"chord_number": [0,3,0,2,0,-1,,,,'row']
				}
			],
			"7(♭9)": [
				{
					"pos": 11,
					"chord_number": _R5_7_9
				},
				{
					"pos": 5,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 11,
					"chord_number": _R5_79
				},
				{
					"pos": 5,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 11,
					"chord_number": _R5_7__9
				},
				{
					"pos": 5,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 11,
					"chord_number": _R5_dim7
				},
				{
					"pos": 4,
					"chord_number": _R6_dim7
				}
			]
		},
		"B♭": {
			"M": [
				{
					"pos": 1,
					"chord_number": _R5_M
				},
				{
					"pos": 6,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 1,
					"chord_number": _R5_M7
				},
				{
					"pos": 6,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 1,
					"chord_number": _R5_7
				},
				{
					"pos": 6,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 11,
					"chord_number": _R5_6
				},
				{
					"pos": 5,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 11,
					"chord_number": _R5_aug
				},
				{
					"pos": 6,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 1,
					"chord_number": _R5_m
				},
				{
					"pos": 6,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 1,
					"chord_number": _R5_mM7
				},
				{
					"pos": 6,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 1,
					"chord_number": _R5_m7
				},
				{
					"pos": 6,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 11,
					"chord_number": _R5_m6
				},
				{
					"pos": 6,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 1,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 5,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 1,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 1,
					"chord_number": _R5_sus4
				},
				{
					"pos": 7,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 1,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 6,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 12,
					"chord_number": _R5_7_9
				},
				{
					"pos": 6,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 12,
					"chord_number": _R5_79
				},
				{
					"pos": 6,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 12,
					"chord_number": _R5_7__9
				},
				{
					"pos": 6,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 12,
					"chord_number": _R5_dim7
				},
				{
					"pos": 5,
					"chord_number": _R6_dim7
				}
			]
		},
		"B": {
			"M": [
				{
					"pos": 2,
					"chord_number": _R5_M
				},
				{
					"pos": 7,
					"chord_number": _R6_M
				}
			],
			"M7": [
				{
					"pos": 2,
					"chord_number": _R5_M7
				},
				{
					"pos": 7,
					"chord_number": _R6_M7
				}
			],
			"7": [
				{
					"pos": 2,
					"chord_number": _R5_7
				},
				{
					"pos": 7,
					"chord_number": _R6_7
				}
			],
			"6": [
				{
					"pos": 12,
					"chord_number": _R5_6
				},
				{
					"pos": 6,
					"chord_number": _R6_6
				}
			],
			"aug": [
				{
					"pos": 12,
					"chord_number": _R5_aug
				},
				{
					"pos": 7,
					"chord_number": _R6_aug
				}
			],
			"m": [
				{
					"pos": 2,
					"chord_number": _R5_m
				},
				{
					"pos": 7,
					"chord_number": _R6_m
				}
			],
			"mM7": [
				{
					"pos": 2,
					"chord_number": _R5_mM7
				},
				{
					"pos": 7,
					"chord_number": _R6_mM7
				}
			],
			"m7": [
				{
					"pos": 2,
					"chord_number": _R5_m7
				},
				{
					"pos": 7,
					"chord_number": _R6_m7
				}
			],
			"m6": [
				{
					"pos": 12,
					"chord_number": _R5_m6
				},
				{
					"pos": 7,
					"chord_number": _R6_m6
				}
			],
			"m7(♭5)": [
				{
					"pos": 2,
					"chord_number": _R5_m7_5
				},
				{
					"pos": 6,
					"chord_number": _R6_m7_5
				}
			],
			"add9": [
				{
					"pos": 2,
					"chord_number": _R5_add9
				}
			],
			"sus4": [
				{
					"pos": 2,
					"chord_number": _R5_sus4
				},
				{
					"pos": 8,
					"chord_number": _R6_sus4
				}
			],
			"7sus4": [
				{
					"pos": 2,
					"chord_number": _R5_7sus4
				},
				{
					"pos": 7,
					"chord_number": _R6_7sus4
				}
			],
			"7(♭9)": [
				{
					"pos": 1,
					"chord_number": _R5_7_9
				},
				{
					"pos": 7,
					"chord_number": _R6_7_9
				}
			],
			"7(9)": [
				{
					"pos": 1,
					"chord_number": _R5_79
				},
				{
					"pos": 7,
					"chord_number": _R6_79
				}
			],
			"7(#9)": [
				{
					"pos": 1,
					"chord_number": _R5_7__9
				},
				{
					"pos": 7,
					"chord_number": _R6_7__9
				}
			],
			"dim7": [
				{
					"pos": 1,
					"chord_number": _R5_dim7
				},
				{
					"pos": 6,
					"chord_number": _R6_dim7
				}
			]
		}
	};

})();


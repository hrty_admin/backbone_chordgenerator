
;(function(window, document){

	"use strict";

	window.DrawChord = {}; // 描画コンストラクタ
	// var Sound = {}; // 音源コンストラクタ

	var _F_B = FINGER_BORD;
	var FLET_X = _F_B.FLET_X,
		FLET_Y = _F_B.FLET_Y,
		CANVAS_WIDTH = _F_B.CANVAS_WIDTH,
		CANVAS_HEIGHT = _F_B.CANVAS_HEIGHT,
		FLET_INTERVAL = _F_B.FLET_INTERVAL,
		STRING_INTERVAL = _F_B.STRING_INTERVAL,
		FLET_NUMBER = _F_B.FLET_NUMBER,
		FLET_LINEWIDTH = _F_B.FLET_LINEWIDTH,
		ARC_RADIUS = _F_B.ARC_RADIUS,
		FLET_COLOR = _F_B.FLET_COLOR,
		STRING_COLOR = _F_B.STRING_COLOR,
		ARC_COLOR = _F_B.ARC_COLOR,
		FLET_NUMBER_COLOR = _F_B.FLET_NUMBER_COLOR;

	// ================================

	DrawChord = function($elm) {
		this.$elm = $elm; // 対象の要素
		this.chords_info = []; // コード情報の配列
		this.canvas_array = [];

		this.div = document.createElement('div'); // コピー用

		this.canvas = document.createElement('canvas'); // コピー用canvas
		this.canvas.setAttribute("width", CANVAS_WIDTH);
		this.canvas.setAttribute("height", CANVAS_HEIGHT);

		this.span = document.createElement('span'); // コピー用コードタイプ
	};

	DrawChord.prototype = {
		setChord: function(root, type) {
			var data = CHORD_CONFIG_DATA,
				chord_pattern = data[root][type],
				i = 0;

			// コードを変更するたびに初期化
			this.$elm.empty();
			this.chords_info = [];
			this.canvas_array = [];
			this.span_array = [];

			for(i; i < chord_pattern.length; i++) {
				this.chords_info.push(chord_pattern[i]);

				this.createCanvas();
				this.setFingerBord(i);
				this.setFletNumber(i);
				this.setChordNumber(i);

				this.setChordType(i);
			}

			this.showCanvas();
		},
		createCanvas: function() {
			var _canvas = this.canvas.cloneNode(true);

			this.canvas_array.push(_canvas);
		},
		setFingerBord: function(n) {
			var _canvas = this.canvas_array[n],
				_ctx = _canvas.getContext('2d'),
				_fletX = FINGER_BORD.FLET_X;

			_ctx.scale(2, 2); // Ratina対応

			// 現在の設定を保存（線の太さ: 1px）
			_ctx.save();

			// フレットの描画
			_ctx.strokeStyle = FLET_COLOR;
			// ローポジション
			if(this.chords_info[n]["pos"] === 1) {
				for(var i = 0; i < FLET_NUMBER+1; i++) {
					if(i === 0) {
						_ctx.lineWidth = FLET_LINEWIDTH*2;
					} else {
						_ctx.lineWidth = FLET_LINEWIDTH;
					}
					_ctx.beginPath();
					_ctx.moveTo(FLET_X + (FLET_INTERVAL*i), FLET_Y);
					_ctx.lineTo(FLET_X + (FLET_INTERVAL*i), FLET_Y + STRING_INTERVAL*5);
					_ctx.closePath();
					_ctx.stroke();
				}
			// ハイポジション
			} else {
				_fletX = FLET_X - FLET_INTERVAL/4;

				_ctx.lineWidth = FLET_LINEWIDTH;
				_ctx.beginPath();
				for(var i = 0; i < FLET_NUMBER+1; i++) {
					_ctx.moveTo(FLET_X + (FLET_INTERVAL*i), FLET_Y);
					_ctx.lineTo(FLET_X + (FLET_INTERVAL*i), FLET_Y + STRING_INTERVAL*5);
				}
				_ctx.closePath();
				_ctx.stroke();
			}

			// 設定をリストア
			_ctx.restore();

			// 弦の描画
			_ctx.strokeStyle = STRING_COLOR;
			_ctx.beginPath();
			for(var i = 0; i < 6; i++) {
				_ctx.moveTo(_fletX, FLET_Y + (STRING_INTERVAL*i));
				_ctx.lineTo(_fletX + (FLET_INTERVAL*FLET_NUMBER + FLET_INTERVAL/2), FLET_Y + (STRING_INTERVAL*i));
			}
			_ctx.closePath();
			_ctx.stroke();

		},
		// フレット番号表示
		setFletNumber: function(n) {
			var _canvas = this.canvas_array[n],
				_ctx = _canvas.getContext('2d'),
				i = 0,
				flet_num = this.chords_info[n]["pos"];

			// フレット数の描画
			_ctx.font = 'bold 18px Verdana';
			_ctx.textAlign = 'center';
			_ctx.fillStyle = FLET_NUMBER_COLOR;
			_ctx.beginPath();
			for(i; i < FLET_NUMBER; i++) {
				_ctx.fillText(flet_num, FLET_X + FLET_INTERVAL + (FLET_INTERVAL*i), FLET_Y + STRING_INTERVAL*5 + 20); // 20は微調整
				++flet_num;
			}
			_ctx.closePath();
		},
		// コード表示
		setChordNumber: function(n) {
			var _canvas = this.canvas_array[n],
				_ctx = _canvas.getContext('2d'),
				i = 0,
				chord_num = this.chords_info[n]["chord_number"],
				flet_num = this.chords_info[n]["pos"];

			// 押弦の描画
			for(var i = 0; i < 6; i++) {
				_ctx.beginPath();
				// 開放弦
				if(flet_num === 1 && chord_num[i] === 0) {
					_ctx.fillStyle = ARC_COLOR;
					_ctx.arc(FLET_X - FLET_INTERVAL/2, FLET_Y + (STRING_INTERVAL*i), ARC_RADIUS, 0, 360);
					_ctx.stroke();
				// ミュート
				} else if(chord_num[i] < 0) {
					_ctx.fillStyle = STRING_COLOR;
					_ctx.fillText("×", FLET_X - FLET_INTERVAL/2, FLET_Y + (STRING_INTERVAL*i) + 6); // 6は微調整

				} else {
					_ctx.fillStyle = ARC_COLOR;
					_ctx.arc(FLET_X - FLET_INTERVAL/2 + FLET_INTERVAL*chord_num[i], FLET_Y + (STRING_INTERVAL*i), ARC_RADIUS, 0, 360);
					_ctx.fill();
				}
				_ctx.closePath();
			}

			// バー
			if(chord_num[6] && chord_num[7] && chord_num[8]) {
				var x_pos = chord_num[6],
					start = chord_num[7] - 1,
					end = chord_num[8] - 1;

				var _fletX = FLET_X - FLET_INTERVAL/4;

				_ctx.strokeStyle = ARC_COLOR;
				_ctx.lineWidth = ARC_RADIUS*2;
				_ctx.beginPath();
				_ctx.moveTo(FLET_X - FLET_INTERVAL/2 + FLET_INTERVAL*x_pos, FLET_Y + (STRING_INTERVAL*start));
				_ctx.lineTo(FLET_X - FLET_INTERVAL/2 + FLET_INTERVAL*x_pos, FLET_Y + (STRING_INTERVAL*end));
				_ctx.closePath();
				_ctx.stroke();
			}

		},
		setChordType: function(n) {
			var _span = this.span.cloneNode(true),
				i = 0,
				chord_num = this.chords_info[n]["chord_number"];

			var chord_type = chord_num[9];

			_span.className = chord_type;
			if(chord_type == 'row') {
				_span.innerHTML = 'ローコード';
			} else if(chord_type == 'root5') {
				_span.innerHTML = '5弦ルート型';
			} else if(chord_type == 'root6') {
				_span.innerHTML = '6弦ルート型';
			};

			this.span_array.push(_span);

		},
		showCanvas: function() {
			var i = 0,
				canvas_array = this.canvas_array,
				span_array = this.span_array,
				$elm = this.$elm;

			for(i; i < canvas_array.length; i++) {
				var div = this.div.cloneNode(true);
				$(div).append(canvas_array[i]).append(span_array[i]);
				$elm.append($(div));
			}
		}
	};

	window.addEventListener('hashchange', function() {
		location.reload();
		window.scroll(0,0);
	}, false);





	// コード進行を返す
	window.getChordProgression = function(scopeChords) {
		var _chordProgression = "",
			i = 0;

		for(i; i < scopeChords.length; i++) {
			if(i > 0) {
				_chordProgression += " - ";
			}
			_chordProgression += scopeChords[i].root.name + scopeChords[i].type.name;
		}

		return _chordProgression;
	};

	window.test = function() {
		return 'test';
	}


})(this, this.document);
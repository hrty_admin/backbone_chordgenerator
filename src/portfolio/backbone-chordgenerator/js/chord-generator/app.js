'use strict';

(function(win, $) {

	var CHORD_CONF = win.CHORD_CONF;
	var INITIAL_CHORD = {
		root: CHORD_CONF.root[0],
		type: CHORD_CONF.type[0]
	}


	// ====================
	// Model
	// ====================

	var Chord = Backbone.Model.extend({});


	// ====================
	// Collection
	// ====================

	var ChordList = Backbone.Collection.extend({
		model: Chord
	});

	var chordList = new ChordList([
		INITIAL_CHORD
	]);


	// ====================
	// View
	// ====================

	var ChordView = Backbone.View.extend({
		tagName: 'li',
		className: 'chord-list',
		collection: chordList,
		events: {
			'change .b_select-root': 'selectRoot',
			'change .b_select-type': 'selectType',
			'click .b_remove-cord': 'destroy'
		},
		initialize: function() {
			_.bindAll(this, 'render', 'changeChord', 'selectRoot', 'selectType', 'showPronunciation', 'destroy', 'remove', 'showRemoveChord');
			this.listenTo(this.model, 'change', this.changeChord);
			this.listenTo(this.model, 'change', this.showPronunciation);
			this.listenTo(this.model, 'destroy', this.remove);
			this.listenTo(this.collection, 'add remove', this.showRemoveChord);
			this.drawChord = {};
		},
		render: function() {
		    var template = _.template($('#chord-list-tmpl').html());
		    var vars = this.model.toJSON();
		    var html = template(vars);
		    this.$el.append(html);

    		this.drawChord = new DrawChord($('.b_chord-canvas', this.$el));
    		this.drawChord.setChord(this.model.toJSON().root.name, this.model.toJSON().type.name);

			this.showPronunciation();
		    
		    return this;
		},
		changeChord: function() {
			this.drawChord.setChord(this.model.toJSON().root.name, this.model.toJSON().type.name);
		},
		selectRoot: function() {
			this.model.set('root', CHORD_CONF.root[$('.b_select-root', this.$el).val()-0]);
		},
		selectType: function() {
			this.model.set('type', CHORD_CONF.type[$('.b_select-type', this.$el).val()-0]);
		},
		showPronunciation: function() {
			$('.b_cord-pronunciation', this.$el).text(this.model.toJSON().root.pronunciation + '・' + this.model.toJSON().type.pronunciation)
		},
		destroy: function() {
			this.model.destroy();
		},
		remove: function() {
			this.$el.remove();
		},
		showRemoveChord: function() {
			if(this.collection.length > 1) {
				$('.b_remove-cord').removeClass('disnone');
				$('.b_remove-cord', this.$el).removeClass('disnone');
			} else {
				$('.b_remove-cord').addClass('disnone');
			}
		}
	});

	var ChordListView = Backbone.View.extend({
		el: '#b_chord-list',
		initialize: function() {
			_.bindAll(this, 'render', 'appendChord');
			this.listenTo(this.collection, 'add', this.appendChord);
			this.render();
		},
		render: function() {
			_(this.collection.models).each(function(chord) {
				this.appendChord(chord);
			}, this);
			return this;
		},
		appendChord: function(chord) {
			var chordView = new ChordView({model: chord});
			this.$el.append(chordView.render().el);
		}
	});


	var ChordProgressionView = Backbone.View.extend({
		el: '#b_chord-progression',
		initialize: function() {
			_.bindAll(this, 'render');
			this.listenTo(this.collection, 'all', this.render);
			this.render();
		},
		render: function() {
			this.$el.text(getChordProgression(this.collection.toJSON()));
			return this;
		}
	});


	var ControllButtonView = Backbone.View.extend({
		el: '#b_btn-wrap',
		events: {
			'click .b_add-chord': 'addChord'
		},
		initialize: function() {
			_.bindAll(this, 'addChord');
		},
		addChord: function() {
			var chord = new Chord();
			chord.set(INITIAL_CHORD);
			this.collection.add(chord);
		}
	});

	var ChordListView = new ChordListView({collection: chordList});
	var chordProgressionView = new ChordProgressionView({collection: chordList});
	var controllButtonView = new ControllButtonView({collection: chordList});




})(this, jQuery);
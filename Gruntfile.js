'use strict';

var path = require('path');

var root = '/portfolio/backbone-chordgenerator';

module.exports = function (grunt) {
	grunt.initConfig({
		// 作業フォルダ、公開&確認フォルダ設定
		connectPaths: {
			srcDir: './src',
			distDir:  './dist',
			buildDir:  './build'
		},
		paths: {
			srcDir: './src' + root,
			distDir:  './dist' + root,
			buildDir:  './build' + root
		},

		// config.rbの設定を記述
		compass: {
			dev: {
				options: {
	                httpPath: '/',
	                cssDir: '<%= paths.distDir %>/css/',
	                sassDir: '<%= paths.srcDir %>/scss/',
	                javascriptDir: '<%= paths.distDir %>/js/',
	                environment: 'development',
	                cache: false
				}
			}
		},

		copy: {
			js: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/js/',
				dest: '<%= paths.distDir %>/js/',
				src: [
				  '**/*.*',
				]
			},
			font: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/font/',
				dest: '<%= paths.distDir %>/font/',
				src: [
				  '**/*.{eot,svg,ttf,woff}'
				]
			},
			html: {
				expand: true,
				dot: true,
				cwd: '<%= paths.srcDir %>/',
				dest: '<%= paths.distDir %>/',
				src: [
				  '**/*.html'
				]
			},
			build: {
				expand: true,
				dot: true,
				cwd: '<%= paths.distDir %>/',
				dest: '<%= paths.buildDir %>/',
				src: [
				  '**/*.*'
				]
			}
		},

		// useminPrepare: {
		// 	options: {
		// 		root: "<%= paths.distDir %>",
		// 		dest: "build"
		// 	},
		// 	html: ["<%= paths.buildDir %>/**/*.html"]
		// },
		// usemin: {
		// 	options: {
		// 		dirs: ["<%= paths.buildDir %>/"]
		// 	},
		// 	html: ["<%= paths.buildDir %>/**/*.html"]
		// },

        connect: {
            dist: {
                options: {
                    port: 9000,
                    base: '<%= connectPaths.distDir %>/'
                }
            },
            build: {
                options: {
                    port: 9001,
                    base: '<%= connectPaths.buildDir %>/',
                    keepalive: true
                }
            }
        },

		esteWatch: {
			options: {
				livereload: {
					enabled: false
				},
				dirs: [
					'<%= paths.srcDir %>/**.html',
					'<%= paths.srcDir %>/scss/**/',
					'<%= paths.srcDir %>/js/**/'
				]
			},
			html: function(filepath) {
				return ['newer:copy:html'];
			},
			scss: function(filepath) {
				return ['compass'];
			},
			js: function(filepath) {
				return ['newer:copy:js'];
			}
		},

		//ファイルの削除
		clean:{
			delDist: ["<%= paths.distDir %>"],
			delBuild: ["<%= paths.buildDir %>"]
		}

	});

	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-este-watch');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-usemin');
	
	grunt.registerTask('default', ['connect:dist', 'esteWatch']);
	grunt.registerTask('compile', ['clean:delDist', 'compass', 'copy:js', 'copy:html', 'copy:font', 'connect:dist', 'esteWatch']);
	grunt.registerTask('build', ['clean:delBuild','compass', 'copy:build', 'connect:build']);
}